module.exports = function (dbConnection) {
    const express = require('express');
    const router = express.Router();

    const jwt = require('jsonwebtoken');
    const fs = require('fs');

    const uploadImage = require('../services/image_upload');

    const publicKey = fs.readFileSync(__dirname + '/../../public.pem', 'utf8');

    router.use((req, resp, next) => {
        jwt.verify(req.cookies.token, publicKey, function(err, decoded) {
            if (err) {
                resp.status(401);
                resp.send('Invalid Token');
            } else {
                req.body.user = decoded;
                if(req.body.user.isModerator || req.body.isAdmin){
                    next();
                } else {
                    resp.status(401);
                    resp.send('You dont have necessary access level')
                }
            }
        });
    })


    /*----ROUTES GOES HERE----*/

    //Upload Image
    router.post('/image', (req, resp) => {
        uploadImage(req, function(err, data) {

            if (err) {
              return res.status(404).end(JSON.stringify(err));
            }
        
            data.link = "http://" + process.env.SERVER_URL + ":" + process.env.APPLICATION_PORT + "/" + data.link;
            // console.log(data);
            res.send(data);
        });
    })

    //Add Article
    router.post('/article', (req, resp) => {
        let body = req.body
        dbConnection.addArticle(body.name, body.content, body.user.id, body.thumbnail)
        .then(res => {
            resp.json(res.rows)
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    });

    //Edit own article
    router.put('/article/:id', (req, resp) => {
        let body = req.body
        dbConnection.updateArticle(body.id, body.name, body.content, body.thumbnail, body.user.id)
        .then(res => {
            resp.json(res.rows)
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    });

    //Delete any comment (Logical Deletion)
    router.delete('/comment/:id', (req, resp) => {
        dbConnection.deleteComment([req.body.user.id, req.params.id, undefined, req.body.reason])
        .then(res => {
            resp.json(res.rows)
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    })

    /*------------------------*/


    dbConnection.test(['Connection Sucessfully passed to the moderator_router'])
    .then(res => {
        console.log(res.rows[0].info);
    })
    .catch(e => {
        console.error('Database connection error:\n', e);
    });   


    return router;
}