module.exports = function(dbConnection){
    const express = require('express');
    const router = express.Router();

    const jwt = require('jsonwebtoken');
    const fs = require('fs');

    const publicKey = fs.readFileSync(__dirname + '/../../public.pem', 'utf8');

    //В теле, в адрессной строке, в куки
    router.use((req, resp, next) => {
        const token = req.body.token || req.query.token || req.cookies.token;
        jwt.verify(token, publicKey, function(err, decoded) {
            if (err) {
                resp.status(401);
                resp.send('Invalid Token');
            } else {
                req.body.user = decoded;
                next();
            }
        });
    }) 


    /*----ROUTES GOES HERE----*/

    //Get Upvotes List
    /*
    /Accepts: 
    /   Article id
    /   User id
    */
   // select * from article_upvotes where id_article = 1 and id_user = 2;
   router.get('/upvotes/:idArticle', (req, resp) => {
        dbConnection.getUpvotesList([req.params.idArticle, req.body.user.id])
        .then(res => {
            resp.json(res.rows)
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
   });

   ///upvotes?id=..
   router.get('/upvotes', (req, resp) => {
       if (!req.query.id) {
           resp.status(404);
           resp.json({e: 'No article id specified'});
       } else {
           resp.redirect('/user/upvotes/' + req.query.id);
       }
   });

    //Add Comment
    /*
    /Accepts:
    /   Article id
    /   User id
    /   Parent id ? Parent id : null
    /   Comment Content
    */
    // insert into comment (id_article, id_user, id_parent, content) values (1, 2, 1, 'test'); 
    router.post('/comment', (req, resp) => {
        dbConnection.addComment([req.body.articleId, req.body.user.id, req.body.parentId, req.body.content])
        .then(res => {
            resp.json(res.rows[0])
        }).catch(e => {
            console.error(e.stack);
            // console.error(e)
            resp.status(500);
            resp.json({e: e.stack});
        })
    })

    //Delete Own Comment
    /*
    /Logical Deletion
    /Accepts:
    /   Comment id
    /   User id
    */
    // update comment set deleted = true, deletion_reason = $4, deleted_by = $1 where id = $2 and id_user = $3;
    // [deleted_by, id, id_user (mb undefined), deletion_reason]
    router.delete('/comment/:id', (req, resp) => {
        dbConnection.deleteComment([req.body.user.id, req.params.id, req.body.user.id, 'Self Deleted'])
        .then(res => {
                resp.json(res.rows[0])
        }).catch(e => {
                resp.status(500);
                resp.json({e: e.stack});
        })
    }) 

    //Rate article
    /*
    /Accepts:
    /   User id
    /   Article id
    /   Rate
    */
    // insert into article_upvotes (id_user, id_article, rate) values (2, 4, 1); 
    //check for -1/1
    router.post('/rate/article', (req, resp) => {
        dbConnection.rateArticle([req.body.user.id, req.body.id, req.body.rate])
        .then(res => {
            resp.json(res.rows[0])
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    })

    //Rate comment
    /*
    /Accepts:
    /   User id
    /   Comment id
    /   Rate
    */
    // insert into comment_upvotes (id_user, id_comment, rate) values (1, 2, 1);
    //check for -1/1
    router.post('/rate/comment', (req, resp) => {
        dbConnection.rateComment([req.body.user.id, req.body.id, req.body.rate])
        .then(res => {
            resp.json(res.rows)
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    })

    /*------------------------*/
    
    dbConnection.test(['Connection Sucessfully passed to the user_router'])
    .then(res => {
        console.log(res.rows[0].info);
    })
    .catch(e => {
        console.error('Database connection error:\n', e);
    });



    return router;
}