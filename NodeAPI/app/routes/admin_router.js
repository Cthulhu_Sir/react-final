module.exports = function (dbConnection) {
    const express = require('express');
    const router = express.Router();

    const jwt = require('jsonwebtoken');
    const fs = require('fs');

    const publicKey = fs.readFileSync(__dirname + '/../../public.pem', 'utf8');

    router.use((req, resp, next) => {
        jwt.verify(req.cookies.token, publicKey, function(err, decoded) {
            if (err) {
                resp.status(401);
                resp.send('Invalid Token');
            } else {
                req.body.user = decoded;
                if(req.body.user.isAdmin){
                    next();
                } else {
                    resp.status(401);
                    resp.send('You dont have necessary access level')
                }
            }
        });
    })


    /*----ROUTES GOES HERE----*/

    //Delete Article
    router.delete('/article/:id', (req, resp) => {
        dbConnection.deleteArticle([req.body.user.id, req.body.reason, req.params.id])
        .then(res => {
            resp.json({status: 'OK'})
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        });
    });

    //Edit Article

    router.put('/article/:id', (req, resp) => {
        dbConnection.editArticle([req.body.name, req.body.content, req.body.thumbnail, req.params.id])
        .then(res => {
            resp.json({status: 'OK'})
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        });
    });


    //Ban User
    router.post('/ban', (req,resp) => {
        dbConnection.banUser([req.body.id, req.body.reason, req.body.user.id])
        .then(res => {
            resp.json({status: 'OK'})
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        });
    });

    //Add Category

    //Delete Category

    //Edit Category

    /*------------------------*/


    dbConnection.test(['Connection Sucessfully passed to the admin_router'])
    .then(res => {
        console.log(res.rows[0].info);
    })
    .catch(e => {
        console.error('Database connection error:\n', e);
    });   


    return router;
}