module.exports = function(dbConnection){
    const express = require('express');
    const router = express.Router();
    
    const fs = require('fs');

    const jwt = require('jsonwebtoken');
    const bcrypt = require('bcryptjs');
    
    const privateKey = fs.readFileSync(__dirname + '/../../private.pem', 'utf8');
    const publicKey = fs.readFileSync(__dirname + '/../../public.pem', 'utf8');

    /*----ROUTES GOES HERE----*/

    //Регистрация
    /*
    /{
    /   email
    /   password
    /   nickname
    /}
    */
    router.post('/signup', (req, resp) => {

        let user = req.body;
        
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, function(err, hash) {
                
                if(err){
                    resp.status(500);
                    resp.json({e: err});
                };

                dbConnection.signup([user.email, hash, user.nickname])
                    .then(res => {
                        resp.status(200);
                        resp.json({
                            status: 'OK',
                            userDate: res.rows[0]
                        });
                    }).catch(e => {
                        console.error(e);
                        resp.status(500);
                        resp.json({e: e.detail});
                    })
            });
        });

    });

    //Авторизация
    router.post('/signin', (req, resp) => {
        let user = req.body;
        let userDB;

        dbConnection.signin([user.email])
            .then(res => {
                if(res.rows.length === 0) throw new Error('Incorrect email');
                userDB = res.rows[0];
                return bcrypt.compare(user.password, userDB.password);
            }).then(res => {
                if (res !== true) throw new Error('Incorrect password');

                jwt.sign({
                    id: userDB.id, 
                    email: userDB.email, 
                    nickname: userDB.nickname,
                    isAdmin: userDB.user_type == 3,
                    isModerator: userDB.user_type == 2
                }, { 
                    key: privateKey,
                    passphrase: process.env.PASSPHRASE 
                }, { 
                    algorithm: 'RS256' 
                }, function(err, token) {
                    if(err) throw new Error(err);

                    resp.status(200);
                    resp.cookie('token', token);
                    resp.json({token: token});
                });

            }).catch(e => {
                console.error(e);
                resp.status(400);
                resp.json({e: e.stack});
            })

    });
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    /*
    /Получить список статей
    /Offset
    /Limit
    /Categories []
    /
    //articles?limit=..&offset=..&cat=..&cat=..
    */
    router.get('/articles', (req, resp) => {
        const categories = req.query.cat || 0;
        const limit = req.query.limit || 10;
        const offset = req.query.offset || 0;

        // console.log(categories, limit, offset);

        dbConnection.getArticles([categories, limit, offset])
        .then(res => {
            if (res.rows.length <= 0) throw new Error("No articles found");
            // console.log(res.rows);
            resp.json(res.rows)
        }).catch(e => {
            console.error(e.stack);
            resp.status(500);
            resp.json({e: e.stack})
        })
    });

    /*
    /Получить одну конкретную статью
    //article?id=..
    /
    /
    */
    router.get('/article', (req, resp) => {
        const id = req.query.id;
        if (id !== null && id !== undefined) {
            resp.redirect('/article/' + id);
        } else {
            resp.status(500);//??
            resp.send({e: new Error('No id specified').stack});
        }
    })

    /*
    //article/2 = /article?id=2
    /
    */
    router.get('/article/:id', (req, resp) => {
        // resp.json(req.params)
        dbConnection.getArticleById([req.params.id])
        .then(res => {
            if(res.rows.length <= 0) throw new Error('No data found');
            resp.json(res.rows[0]);
        }).catch(e => {
            console.error(e.stack);
            resp.status(500);
            resp.json({e: e.stack});
        })
    });

    /*
    /Принмает id статьи, возвращает список комментариев.
    //comments?id=..
    /
    /
    */
    router.get('/comments', (req, resp) => {
        const articleId = req.query.id;
        if (articleId === null || articleId === undefined){
            resp.status(404);
            resp.json({e: 'No comments found for this article'});
        }
        dbConnection.getCommentsTree([articleId])
        .then(res => {
            if (res.rows.length <= 0) throw new Error('No comments found for this article'); 
            resp.json(res.rows[0]["get_article_comments_tree"]);
        }).catch(e => {
            resp.status(500);
            resp.json({e: e.stack});
        })
    });

    router.get('/comments/:id', (req, resp) => {
        resp.redirect('/comments?id=' + req.params.id);
    });

    router.get('/categories', (req, resp) => {
        dbConnection.getCategories()
        .then(res => {
            resp.json(res.rows);
        }).catch(e => {
            console.error(e.stack);
            resp.status(500);
            resp.json({e: e.stack});
        })
    });
    /*------------------------*/


    dbConnection.test(['Connection Sucessfully passed to the page_router'])
    .then(res => {
        console.log(res.rows[0].info);
    })
    .catch(e => {
        console.error('Database connection error:\n', e);
    });   

    function isUser(token){
        return jwt.verify(token, publicKey, function(err, decoded) {
            if (err) {
                return false;
            } else {
                return true;
            }
        });
    }

    
    return router;
}