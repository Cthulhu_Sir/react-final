require('dotenv').config();

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const fileUpload = require('express-fileupload');

const dbConnection = require('./database');

const homeRouter = require("./routes/page_router")(dbConnection);
const adminRouter = require("./routes/admin_router")(dbConnection);
const userRouter = require("./routes/user_router")(dbConnection);
const moderatorRouter = require("./routes/moderator_router")(dbConnection);

app.use(express.static("./public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(fileUpload());

app.set("view engine", "ejs");
app.set("views", __dirname + "./../public/html");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

console.log('setting routes');

app.use('/', homeRouter);

app.use('/user', userRouter);

app.use('/moder', moderatorRouter);

app.use('/admin', adminRouter);

app.use((req, resp) => {
    resp.send("404 Page Not Found. But why?");
});

app.listen(process.env.APPLICATION_PORT, () => {
    console.log(`Application is up and Running on port ${process.env.APPLICATION_PORT}`);
});
