/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){
    const pool = this.pool;
    return pool.query(`SELECT a.id
    , a.name
    , a.content
    , a.rating
    , a.id_user as author
    , u.nickname as author_nickname
    , a.thumbnail
    , a.date
    , json_agg(json_build_object('id', c2.id, 'name', c2.name)) as categories
    FROM article a
    INNER JOIN article_category a2 on a.id = a2.id_article
    INNER JOIN category c2 on a2.id_category = c2.id
    INNER JOIN users u on a.id_user = u.id
    WHERE a.id = $1::bigint
    AND a.deleted = FALSE
    GROUP BY a.id, a.name, a.content, a.rating, a.id_user, u.nickname, a.thumbnail, a.date;`, params);
}

module.exports = execute;