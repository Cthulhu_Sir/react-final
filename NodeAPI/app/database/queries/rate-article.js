/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){
    const pool = this.pool;
    return pool.query(`insert into article_upvotes (id_user, id_article, rate)
     values ($1::bigint, $2::bigint, $3::smallint) RETURNING *`, params);
}

module.exports = execute;