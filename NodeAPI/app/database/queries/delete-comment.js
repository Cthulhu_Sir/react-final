/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){
    let userId = params[2] === undefined || params[2] === null ? '' : 'and id_user = $3::bigint'
    const pool = this.pool;
    return pool.query(`update comment 
    set deleted = true, 
    deletion_reason = $4::text, 
    deleted_by = $1::bigint where id = $2::bigint `
    + userId + ' RETURNING id', params);
}

module.exports = execute;