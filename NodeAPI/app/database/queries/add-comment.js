/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){

    params[2] = (params[2] === null || params[2] === undefined) ? null : params[2];
    params[3] = (params[3] === null || params[3] === undefined) ? null : params[3];

    const pool = this.pool;
    return pool.query(`insert into comment (
            id_article
            , id_user
            , id_parent
            , content)
        values (
            $1::bigint
            , $2::bigint
            , $3::bigint
            , $4::text 
            ) RETURNING id`, params);
}

module.exports = execute;