/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){
    const pool = this.pool;
    return pool.query('INSERT INTO users (email, password, nickname) VALUES ($1::text, $2::text, $3::text) RETURNING id, email', params);
}

module.exports = execute;