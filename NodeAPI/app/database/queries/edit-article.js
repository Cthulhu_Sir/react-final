/*
*   Каждый запрос должен быть в своём JS файле
*   
*   Всегда должен экспортироваться только один метод
*   Входные параметры метода: params - массив параметров для выполнения запроса
*   
*   pool передаётся в запрос автоматически
*
*   Не забываем "регистрировать" запрос в queries.json
*
*   Имя запроса задаётся как раз таки в queries.json
*
*/

function execute(params){
    const pool = this.pool;
    return pool.query(`UPDATE article
        SET name = $1::text,
            content = $2::text,
            thumbnail = $3::text
        WHERE id = $4::bigint;`, params);
}

module.exports = execute;