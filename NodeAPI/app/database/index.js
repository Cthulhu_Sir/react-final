const { Pool, Client } = require('pg');
const pool = new Pool();

const fs = require('fs');
const pathes = JSON.parse(fs.readFileSync(__dirname + '/queries.json', 'utf8'));

let queries = {};
for (key in pathes){
    queries[key] = require(pathes[key]).bind({pool: pool});
}

/*--------------------------*/

queries.test(['DB connection successful'])
    .then(res => {
        console.log(res.rows[0].info);
    })
    .catch(e => {
        console.error('Database connection error:\n', e);
    });    

/*--------------------------*/

module.exports = queries;