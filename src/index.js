import React from 'react';
import ReactDOM from 'react-dom';
import '../public/css/index.css';
import '../public/css/style.css';
import '../public/css/all.min.css';
import App from './components/App';
import { BrowserRouter } from 'react-router-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers'

const rootElement = document.getElementById('root');
const store = createStore(reducers, applyMiddleware(thunk));

console.log(store);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider >
    , rootElement);
