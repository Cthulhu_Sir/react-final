import api from '../apis/api';

export const fetchArticles = () => async dispatch => {
    const response = await api.get('/articles');
    dispatch({ type: 'FETCH_ARTICLES', payload: response.data });
}

export const fetchCategories = () => async dispatch => {
    const response = await api.get('/categories');
    dispatch({ type: 'FETCH_CATEGORIES', payload: response.data })
}

export const fetchArticle = (id) => async dispatch => {
    const response = await api.get('/article/' + id)
    console.log('Create action "FETCH_ARTICLES" with payload: ', [response.data])
    dispatch({ type: 'FETCH_ARTICLES', payload: [response.data]})
}