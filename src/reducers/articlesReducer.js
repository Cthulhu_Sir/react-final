export default (state=[], action) => {
    // console.log('REDUCER STATE: ', state, '\nREDUCER ACTION: ', action)
    console.log("Articles Reducer. Action: ", action)
    switch(action.type){
        case 'FETCH_ARTICLES': return action.payload;
        default: return state;
    }
}