import React from 'react';
import {FacebookShareButton,LinkedinShareButton,TwitterShareButton,VKShareButton,FacebookIcon,TwitterIcon,LinkedinIcon,VKIcon} from 'react-share';

const title = "Game news";
const shareUrl = `https://www.example.com${window.location.pathname}`;
const iconSize = 48;

class Share extends React.Component {
    render() {
        return (
<ul className="list-inline">

                <li className="list-inline-item">
                <FacebookShareButton
                url={shareUrl}
                quote={title}
                >
                <FacebookIcon
                  size={iconSize}
                  round={false} />
              </FacebookShareButton>
{/*  А нужен ли каунтер...?
              <FacebookShareCount
                url={shareUrl}
                className="count">
                {count => count}
              </FacebookShareCount> */}
                </li>
                <li className="list-inline-item">
                <TwitterShareButton
                url={shareUrl}
                title={title}>
                <TwitterIcon
                  size={iconSize}
                  round={false} />
              </TwitterShareButton>
                </li>
                <li className="list-inline-item">
                <LinkedinShareButton
                url={shareUrl}
                title={title}
                windowWidth={750}
                windowHeight={600}>
                <LinkedinIcon
                  size={iconSize}
                  round={false} />
              </LinkedinShareButton>
                </li>
                <li className="list-inline-item">
                <VKShareButton
                url={shareUrl}
                title={title}
                windowWidth={750}
                windowHeight={600}>
                <VKIcon
                  size={iconSize}
                  round={false} />
              </VKShareButton>
                </li>
                </ul>
                        )
                    }
                }
                
                export default Share;