import React from 'react';
import { Link } from "react-router-dom";

class Banner extends React.Component {
    render() {
        return (
            <section className="banner">
                <div className="container">
                    <div className="row">
                        <div className="mt-5 col-12">
                            <Link to={`#`}>
                                <img style={{ height: "270px", display: "block" }} className="img-fluid" src="#" alt="" />
                                <div className="json" />
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Banner;