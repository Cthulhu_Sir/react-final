import React from 'react';
// import {Link} from "react-router-dom";

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row footer-style text-center">
                        <div className="col-12 font-weight-light my-3 logo">GAME<strong className="font-weight-bold">NEWS</strong>
                        </div>
                        <div className="col-12 text-uppercase">
                            <ul className="list-inline">
                                <li className="list-inline-item">
                                    <a href="/">facebook</a></li>
                                <li className="list-inline-item">
                                    <a href="/">twitter</a></li>
                                <li className="list-inline-item">
                                    <a href="/">instagram</a></li>
                                <li className="list-inline-item">
                                    <a href="/">vk</a></li>
                                <li className="list-inline-item">
                                    <a href="/">rss</a></li>
                            </ul>
                        </div>
                        <div className="col-12 copyright mt-2 mb-5">Copyright 2019. <span className="text-uppercase">all rights
                        reserved.</span></div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;