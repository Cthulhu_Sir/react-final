import React from 'react';
// import $ from 'jquery';
// import { Link } from "react-router-dom";
import FeedArticle from './FeedArticle';

import { fetchArticles } from '../actions';
import { connect } from 'react-redux';

class RecentNews extends React.Component {

  componentDidMount() {
    this.props.fetchArticles();
  }

  render() {
    const isLoading = this.props.articles.length === 0;
    const articles = this.props.articles;
    return (
      <div className="col-8 ">
        <div className="row recent-news">
          <div className="col-12">
            <h4 className="text-dark font-weight-bold section-title my-5 py-3">
              Recent news
            </h4>
          </div>
          <React.Fragment>
            {!isLoading ? (
              articles.map(element => {
                return (
                  <FeedArticle
                    key={`article-${element.id}`}
                    id={element.id}
                    name={element.name}
                    date={element.date}
                    content={element.content}
                    thumbnail={element.thumbnail}
                  />
                )
              })
            ) : (
                <h3>Loading...</h3>
              )
            }
          </React.Fragment>

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state => {
  return ({ articles: state.articles })
})

export default connect(mapStateToProps, { fetchArticles })(RecentNews);