import React from 'react';
// import {Link} from "react-router-dom";

class Features extends React.Component {
    render() {
        return (
            <div className="row p-4">
                <div className="col-8 d-flex align-items-start flex-column features-1 p-4">
                    <div className="category mt-auto">NEWS</div>
                    <div className="title">Disco Elysium Confirmed for PS4 and Xbox One</div>
                    <div className="date">3 November 2019</div>
                </div>
                <div className="col-4 px-0">
                    <div className="col-12 p-4 d-flex align-items-start flex-column features-2">
                        <div className="category mt-auto">NEWS</div>
                        <div className="title">Blizzard Announces Overwatch 2 at BlizzCon</div>
                        <div className="date">3 November 2019</div>
                    </div>
                    <div className="col-12 p-4 d-flex align-items-start flex-column features-3">
                        <div className="category mt-auto">NEWS</div>
                        <div className="title">Don't Expect Diablo 4 Too Soon</div>
                        <div className="date">3 November 2019</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Features;