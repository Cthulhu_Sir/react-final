import React from 'react';
// import {Link} from "react-router-dom";
import $ from 'jquery';

import CarouselItem from './CarouselElement';

import { fetchArticles } from '../actions';
import { connect } from 'react-redux';

class Carousel extends React.Component {

    state = {
        active: 0,
    }

    componentDidMount() {
        console.log('Carousel Mounted');
        this.props.fetchArticles();
    }

    nextSlide = () => {
        const index = (this.state.active + 1 >= this.props.articles.length - 4) ? 0 : this.state.active + 1;
        this.setState({active: index});
        return index;
    }

    prevSlide = () => {
        const index = (this.state.active - 1 <= 0 ) ? this.props.articles.length - 4 : this.state.active - 1;
        this.setState({active: index});
        return index;
    }

    render() {
        console.log('Carousel Rendered');
        return (
            <div className="row">
                <div className="row no-gutters">
                    <div id="hotnews" className="carousel slide" data-ride="carousel" data-interval="9000">
                        <div className="carousel-inner row w-100 mx-auto carousel-news" role="listbox">
                            {this.props.articles.map((el, i) => {
                                let active = false;
                                if (i == this.state.active) {
                                    active = true;
                                }
                                return (
                                    <CarouselItem 
                                        key={`carousel-item-${el.id}`}
                                        active={active} 
                                        title={el.name} 
                                        date={el.date} 
                                        thumbnail={el.thumbnail} 
                                    />
                                )
                            })}
                        </div>
                        <a href={`#hotnews`}
                            className="carousel-control-prev"
                            role="button"
                            onClick={this.prevSlide}
                            data-slide-to={this.state.active}>
                            <span className="carousel-control-prev-icon" aria-hidden="true" />
                            <span className="sr-only">Previous</span>
                        </a>
                        <a href={`#hotnews`}
                            className="carousel-control-next text-faded"
                            role="button"
                            onClick={this.nextSlide}
                            data-slide-to={this.state.active}>
                            <span className="carousel-control-next-icon" aria-hidden="true" />
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

// export default Carousel;

const mapStateToProps = (state => {
    return ({ articles: state.articles })
})
  
export default connect(mapStateToProps, { fetchArticles })(Carousel);