import React from 'react';
import NavBar from './NavBar';
import Banner from './Banner'
import Footer from './Footer';
import Share from './Share';
// import axios from 'axios';
import { connect } from 'react-redux';
import { fetchArticle } from '../actions';

import * as HtmlParser from 'html-to-react';
import 'froala-editor/css/froala_style.min.css';


class Single extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      image: '',
    }
  }

  componentDidMount() {
    console.log('After component did mount');
    console.log('this.props.data: ', this.props.data);
    if (this.props.data === undefined) {
      console.log('Data is empty.\nFetch new data');
      const id = new URL(window.location.href).searchParams.get('id');
      this.props.fetchArticle(id);
    }
  }

  renderPage() {
    const parser = new HtmlParser.Parser();
    const renderedContent = parser.parse(this.props.data.content);
    return (
      <>
        <NavBar />
        <Banner />
        <div className="container" id="content">
          <h1>{this.props.data.name}</h1>
          <img alt="{image}" className="img-fluid w-100" src={"http://91.218.230.193:4000/img/thumbnails/" + this.props.data.thumbnail} />
          <Share />
          <ul className="list-inline">
            <li className="list-inline-item">{this.props.data.categories[0].name}</li>
            <li className="list-inline-item">by {this.props.data.author_nickname}</li>
            <li className="list-inline-item">Posted {this.props.data.date}
            </li>
          </ul>
          <div className="content fr-view">{renderedContent}</div>
        </div>
        <Footer />
      </>
    )
  }

  render() {
    console.log('Rendering Component. Data: ', this.props.data);
    console.log('Passing check: ', this.props.data === undefined)
    return (this.props.data === undefined ?
      <h1>Loading...</h1> : this.renderPage()
    );
  }


}

const mapStateToProps = (state) => {
  const id = new URL(window.location.href).searchParams.get('id');
  const retVal = state.articles.filter(el => el.id === id)[0];
  return ({ data: retVal === undefined || retVal === null ? undefined : retVal })
}

export default connect(mapStateToProps, { fetchArticle })(Single);