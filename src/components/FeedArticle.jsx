import { Component } from 'react';
import React from 'react';
import { Link } from 'react-router-dom';

import * as HtmlParser from 'html-to-react';
import 'froala-editor/css/froala_style.min.css';

class FeedArticle extends Component {

    render() {
        const parser = new HtmlParser.Parser();
        return (
            <Link className="row pb-4" to={`/single?id=` + this.props.id}>
                <div className="col-4 pb-4">
                    <img className="img-fluid" src={"http://91.218.230.193:4000/img/thumbnails/" + this.props.thumbnail} alt="image1" />
                </div>
                <div className="col-8">
                    <div className="text-danger">news</div>
                    <div className="text-dark font-weight-bold">{this.props.name}</div>
                    <div className="date">{this.props.date}</div>
                    <div className="description fr-view">{parser.parse(this.props.content)}</div>
                </div>
            </Link>
        )
    }
}

export default FeedArticle;