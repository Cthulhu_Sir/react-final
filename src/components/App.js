import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Feed from './Feed';
import Single from './Single';
import Login from './login';

const App = () => {
    return (
        <div className="ui segment">
            <Switch>
                <Route exact path='/' component={Feed}/>
                <Route path='/single' component={Single}/>
                <Route path='/login' component={Login}/>
            </Switch>
        </div>
    );
};

export default App;
