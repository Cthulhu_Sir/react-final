import React from 'react';
import { Link } from "react-router-dom";

import { fetchCategories } from '../actions';
import { connect } from 'react-redux';

class NavBar extends React.Component {
  state = {
    isLoading: true,
    categories: [],
    error: null
  };

  // fetchCategories() {
  //   fetch(`http://91.218.230.193:4000/categories`)
  //     .then(response => response.json())
  //     .then(data =>
  //       this.setState({
  //         categories: data,
  //         isLoading: false,
  //       })
  //     )
  //     .catch(error => this.setState({ error, isLoading: false }));
  // }

  componentDidMount() {
    // this.fetchCategories();
    this.props.fetchCategories();
  }
  render() {
    // const { isLoading, categories, error } = this.state;
    const categories = this.props.categories;
    const isLoading = categories.length === 0
    return (
      <div>
        <Link to={`/#`} className="bg-container">
          <div className="background position-absolute mt-5" />
        </Link>
        <nav className="navbar fixed-top navbar-light navbar-expand-md bg-light justify-content-center">
          <div className="container">
            <Link to={`/`} className="navbar-brand mr-0 logo font-weight-light">
              GAME<strong className="font-weight-bold">NEWS</strong>
            </Link>
            <button className="navbar-toggler ml-1" type="button" data-toggle="collapse"
              data-target="#collapsingNavbar2">
              <span className="navbar-toggler-icon" />
            </button>
            <div className="navbar-collapse collapse justify-content-between align-items-center w-100"
              id="collapsingNavbar2">
              <ul className="navbar-nav mx-5 px-5 text-center justify-content-between align-items-center w-100">
                <React.Fragment>
                  {/* {error ? <p>{error.message}</p> : null} */}
                  {!isLoading ? (
                    categories.map(category => {
                      const { id, name } = category;
                      return (
                        <div key={id}>
                          <li className="nav-item active">
                            <Link to={name} className="nav-link">{name}</Link>
                          </li>
                        </div>
                      );
                    })
                  ) : (
                      <h3>Loading...</h3>
                    )
                  }
                </React.Fragment>
              </ul>
              <Link to={`login`} className="nav-link">
                <button type="button" className="btn btn-danger">Login</button>
              </Link>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return ({ categories: state.categories })
}

// export default NavBar;
export default connect(mapStateToProps, { fetchCategories })(NavBar);