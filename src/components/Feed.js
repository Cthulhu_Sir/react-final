import React from 'react';
import NavBar from './NavBar';
import Banner from './Banner'
import Features from './Features';
import Carousel from './Carousel';
import RecentNews from './RecentNews';
import Sidebar from './Sidebar'
import Videos from './Videos';
import Footer from './Footer';

const Feed = () => {
    return (
        <div>
            <NavBar />
            <Banner />
            <div className="container" id="content">
                <Features />
                <Carousel/>
                <div className="row">
                    <RecentNews />
                    <Sidebar />
                </div>
                <Videos />
            </div>
            <Footer />
        </div>
    )
};

export default Feed;