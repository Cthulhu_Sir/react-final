import React from 'react';
import { Component } from 'react';

class CarouselElement extends Component {
    render() {
        console.log('Carousel Item Rendered');
        return (
            <div className={`carousel-item col-3 ${this.props.active ? 'active' : ''}`}>
                <div className="panel panel-default">
                    <div className="panel-thumbnail">
                        <a href={'/'} title="image 1" className="thumb">
                            <div className="post-info d-flex align-items-start flex-column p-2">
                                <div className="category mt-auto text-uppercase">news</div>
                                <div className="title">{this.props.title}</div>
                                <div className="date">{this.props.date}</div>
                            </div>
                            <img className="img-fluid mx-auto d-block"
                                src={`http://91.218.230.193:4000/img/thumbnails/${this.props.thumbnail}`}
                                alt="slide 1" />
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default CarouselElement;