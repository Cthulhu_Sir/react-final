import React from "react";
// import {Link} from "react-router-dom";

class Sidebar extends React.Component {
    render() {
        return (
            <div className="col-4">
            <div className="sidebar my-5">
                {/* 
                    <div className="font-weight-bold py-3 sidebar-title text-uppercase">
                        <span className="text-danger">Most popular </span>
                        <span>week </span>
                        <span>month </span>
                        <span>all </span>
                    </div>
                </div> */}
                <div className="card-header tab-card-header">
          <ul className="nav nav-tabs card-header-tabs text-uppercase font-weight-bold" id="myTab" role="tablist">
            <li className="nav-item">
                <a className="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Week</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Month</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">All</a>
            </li>
          </ul>
        </div>
        <div className="tab-content" id="myTabContent">
          <div className="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
            <h5 className="card-title">Week</h5>          
          </div>
          <div className="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
            <h5 className="card-title">Month</h5>          
          </div>
          <div className="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
            <h5 className="card-title">All</h5>          
          </div>

        </div>
        </div>
            </div>
        )
    }
}

export default Sidebar;